# Introduction

This repo contains all the necessary artifacts to go from zero to demo Anthos Service Mesh 1.10.4-14 in a multicluster setup. See [Add GKE clusters to Anthos Service Mesh](https://cloud.google.com/service-mesh/docs/gke-install-multi-cluster) and [Istio Install Multi-Primary](https://istio.io/latest/docs/setup/install/multicluster/multi-primary/) for additional details.

The specific multicluster deployed here follows the multimaster architecture blueprint documented in the [Istio documentation page](https://istio.io/latest/docs/setup/install/multicluster/multi-primary/):

![Istio multimaster setup](https://istio.io/latest/docs/setup/install/multicluster/multi-primary/arch.svg)

# Installing

Install by using the `install` option that installs two zonal clusters in the same project and region. The script then deploys Anthos Service Mesh with the automated Google official script in both clusters. `install` will enable endpoint discovery as well by cross installing secrets that will then provide access to the opposite cluster's API server:

```bash
./gke-asm-multi install
```

Default installed ASM version can be configured by changing the env. variables inside the script. This is something that you should do if you want to test an older ASM version, of a newer one if I haven't updated this guide fast enough :-)

# Doing the multicluster setup
Now, you'll use another automated script to deploy a sample workload in both clusters that will allow you to test what we've just configured in the previous step.

## Testing sidecar injection
Locate the revision label value from `istiod` service:
```bash
kubectl -n istio-system get pods -l app=istiod --show-labels
```
```text
NAME                                READY   STATUS    RESTARTS   AGE   LABELS
istiod-asm-191-1-65579b7f74-lmsjr   1/1     Running   0          39m   app=istiod,install.operator.istio.io/owning-resource=unknown,istio.io/rev=asm-191-1,istio=istiod,operator.istio.io/component=Pilot,pod-template-hash=65579b7f74,sidecar.istio.io/inject=false
istiod-asm-191-1-65579b7f74-pb4r4   1/1     Running   1          39m   app=istiod,install.operator.istio.io/owning-resource=unknown,istio.io/rev=asm-191-1,istio=istiod,operator.istio.io/component=Pilot,pod-template-hash=65579b7f74,sidecar.istio.io/inject=false
```

This should be happening for your second cluster (`gke-asm-2` if you haven't changed the names) as this has been the last context enabled by the installation script. But because both clusters are configured the same, whatever you get here should also be valid for the other cluster.

Note the value of the `istiod` revision label, which follows the prefix `istio.io/rev=`. For ASM 1.9.1 rev1, it should be **`asm-191-1`**.

Go ahead execute the script `supplemental`:

```bash
./supplemental
```

The script performs the following tasks

- Create a `sample` namespace in both clusters.
- Overwrite the revision label with the correct one you just got from the previous command.
- Create the sample `HelloWorld` service in both clusters.
- Deploy `HelloWorld v1` to Cluster 1 and `HelloWorld v2` to Cluster 2.

The `helloworld` service is defined as follows:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: helloworld
  labels:
    app: helloworld
    service: helloworld
spec:
  ports:
  - port: 5000
    name: http
  selector:
    app: helloworld
```

The service is picking up whatever pods have the `app: helloworld` label.

In order to make the `HelloWorld` service callable from any cluster, the DNS lookup must succeed in each cluster. We could have deployed a common DNS service for both clusters, but instead we will address this by deploying the HelloWorld Service to each cluster in the mesh.

The two deployments deployed by the script, `helloworld-v1` and `helloworld-v2` are defined as follows:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: helloworld-v1
  labels:
    app: helloworld
    version: v1
spec:
  replicas: 1
  selector:
    matchLabels:
      app: helloworld
      version: v1
  template:
    metadata:
      labels:
        app: helloworld
        version: v1
    spec:
      containers:
      - name: helloworld
        image: docker.io/istio/examples-helloworld-v1
        resources:
          requests:
            cpu: "100m"
        imagePullPolicy: IfNotPresent #Always
        ports:
        - containerPort: 5000
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: helloworld-v2
  labels:
    app: helloworld
    version: v2
spec:
  replicas: 1
  selector:
    matchLabels:
      app: helloworld
      version: v2
  template:
    metadata:
      labels:
        app: helloworld
        version: v2
    spec:
      containers:
      - name: helloworld
        image: docker.io/istio/examples-helloworld-v2
        resources:
          requests:
            cpu: "100m"
        imagePullPolicy: IfNotPresent #Always
        ports:
        - containerPort: 5000
```

Both label the pods in the deployment with `app: helloworld` so they can be targeted by the service.

Now, confirm both services are running by getting the pods from the deployments, for both cluster 1 and 2:

```bash
contexts=( gke-asm-1 gke-asm-2)
for CTX in "${contexts[@]}"; do
   echo "Getting pods for cluster ${CTX}":
   kubectl get pod --context=${CTX} -n sample --show-labels
done
```

## Testing multicluster mesh setup

First, move to the istio directory and capture it in a variable:

```bash
cd istio-*
export PROJECT_DIR=$(pwd)
```

Then, deploy the sleep service application in both clusters. It will be useful right below to call the service from both clusters:

```bash
for CTX in "${contexts[@]}"; do
    kubectl apply --context=${CTX} \
      -f ${PROJECT_DIR}/samples/sleep/sleep.yaml -n sample
done
```

Check that the pods are running:

```bash
for CTX in "${contexts[@]}"; do
    kubectl get pod --context=${CTX} -n sample -l app=sleep
done
```

Lastly, to verify that cross-cluster load balancing works as expected, call the `HelloWorld` service several times using the `Sleep` pod. We will do that from both clusters and will expect responses from both v1 and v2 versions of the pods implenting the service:

```bash
for CTX in "${contexts[@]}"; do
   echo "Testing service invocation from cluster ${CTX}"
   for in in {1..10}; do
   kubectl exec --context=${CTX} -it -n sample -c sleep \
      $(kubectl get pod --context=${CTX} -n sample -l \
      app=sleep -o jsonpath='{.items[0].metadata.name}') -- curl \
      helloworld.sample:5000/hello
   done
done
```

As mentioned, the `helloworld` version should toggle between `v1` and `v2`:

```text
Hello version: v2, instance: helloworld-v2-758dd55874-6x4t8
Hello version: v1, instance: helloworld-v1-86f77cd7bd-cpxhv
```

# Tearing down the environment

When you're done with the demo, run:

```bash
./gke-asm-multi destroy
```

# Todo

- Add supplemental script as an option to main installation script.