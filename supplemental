#!/usr/bin/env bash
# Rest of the installation, to be done properly
set -x

readonly ISTIO_VER=1.9.1
readonly ASM_SUBVER=1
readonly __SCRIPT_DIR=$(cd "$(dirname "$0")" && pwd)
readonly __ASM_VER_STRING=${ISTIO_VER}-asm.${ASM_SUBVER}
readonly __ASMV="asm-$(echo "${ISTIO_VER}" | tr -d '.')-${ASM_SUBVER}"
PROJECT_DIR="${__SCRIPT_DIR}"/istio-"${__ASM_VER_STRING}"
CLUSTER_NAME_1=gke-asm-1
CLUSTER_ZONE_1=europe-west1-b
CLUSTER_NAME_2=gke-asm-2
CLUSTER_ZONE_2=europe-west1-d
CLUSTER_NAMES_CLUSTER_ZONES=( "${CLUSTER_NAME_1},${CLUSTER_ZONE_1}" "${CLUSTER_NAME_2},${CLUSTER_ZONE_2}" )

config_discovery() {
    # Configure endpoint discovery between CLUSTER_NAMES_CLUSTER_ZONES
    "${PROJECT_DIR}"/bin/istioctl x create-remote-secret --context="${CLUSTER_NAME_1}" --name="${CLUSTER_NAME_1}" | \
    kubectl apply -f - --context="${CLUSTER_NAME_2}"
    "${PROJECT_DIR}"/bin/istioctl x create-remote-secret --context="${CLUSTER_NAME_2}" --name="${CLUSTER_NAME_2}" | \
    kubectl apply -f - --context="${CLUSTER_NAME_1}"
}

install_hello_world_service() {
  for CLUSTER_NAME_CLUSTER_ZONE in "${CLUSTER_NAMES_CLUSTER_ZONES[@]}"; do
    IFS=',' read -r CLUSTER_NAME CLUSTER_ZONE <<< "${CLUSTER_NAME_CLUSTER_ZONE}"   
    kubectl create --context="${CLUSTER_NAME}" namespace sample
    kubectl label --context="${CLUSTER_NAME}" namespace sample \
      istio-injection- istio.io/rev="${__ASMV}" --overwrite
    kubectl create --context="${CLUSTER_NAME}" \
      -f "${PROJECT_DIR}"/samples/helloworld/helloworld.yaml \
      -l service=helloworld -n sample
  done
}

deploy_hello_world_app() {
  # Deploy Hello World v1 to cluster 1
  kubectl create --context=${CLUSTER_NAME_1} \
  -f "${PROJECT_DIR}"/samples/helloworld/helloworld.yaml \
  -l app=helloworld -l version=v1 -n sample
  # Deploy Hello World v2 to cluster 2
  kubectl create --context=${CLUSTER_NAME_2} \
  -f "${PROJECT_DIR}"/samples/helloworld/helloworld.yaml \
  -l app=helloworld -l version=v2 -n sample
}

main() {
  config_discovery
  install_hello_world_service
  deploy_hello_world_app
}

main "${@}"
